=== ACF OpenStreetMap Field into a Block ===
Contributors: julianoe
Stable tag: 1.0
Tested up to: 5.7.1

Very simple plugin that adds an OpenStreetMap ACF block to the WordPress block editor.

== Description ==
Very simple plugin to add Acf Block support for the <a href="https://wordpress.org/plugins/acf-openstreetmap-field/">ACF OpenStreetMap Field</a> from Jörn Lund.
This plugin obviously will only work if you install Advanced Custom Field and ACF OpenStreetMap Field.

The plugin will create an ACF group field with one OpenStreetMap field configured with default parameters.
You can always override this ACF Group field by creating your own group field titled "ACF OSM BLOCK" and defining its location to be the "ACF OpenStreetMap Block".

== Changelog ==

- 1.0 - 2021/05/07
Initial publication of the plugin


== Credits ==
Plugin working with <a href="https://profiles.wordpress.org/podpirate/">podpirate</a> aka Jörn Lund's plugin
Photo by <a href="https://www.pexels.com/@oidonnyboy" target="_blank">Nick Wehrli</a> from pexels.
Icon "location" by the Wordpress Dashicons.
